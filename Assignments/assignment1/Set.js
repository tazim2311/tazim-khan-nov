class Set{
    constructor(){
        this.values = [];
        this.numberOfValues = 0;
    }

    add(value){
        if(!~this.values.indexOf(value)) {
            this.values.push(value);
            this.numberOfValues++;
          }
    }

    remove(value){
        var index = this.values.indexOf(value);
    if(~index) {
    this.values.splice(index, 1);
    this.numberOfValues--;
    }
    }
    contains(value){
        return this.values.indexOf(value) !== -1;
    }

    union(set){
        var newSet = new Set();
  set.values.forEach(function(value) {
    newSet.add(value);
  });
  this.values.forEach(function(value) {
    newSet.add(value);
  });
  return newSet;
    }

    Intersect(set){
        var newSet = new Set();
        this.values.forEach(function(value) {
          if(set.contains(value)) {
            newSet.add(value);
          }
        });
        return newSet;
    }

    difference(set){
      var newSet = new Set();
      this.values.forEach(function(value) {
      if(!set.contains(value)) {
      newSet.add(value);
     }
     });
    return newSet;
    }

    isSubset(set){
      return set.values.every(function(value) {
        return this.contains(value);
      }, this);
    }

    length(){
        return this.numberOfValues;
    }
    print(){
        console.log(this.values.join(' '));
    }
}
var set1 = new Set();
set1.add(1);
set1.add(2);
set1.add(3);
set1.add(1);
set1.add(5);
set1.print();
set1.remove(3);
set1.print();
var set2= new Set();
set2.add(10);
set2.add(11);
set2.add(12);
set2.print();
var set3=set2.union(set1);
set3.print();


